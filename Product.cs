﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaShop
{
    public class Product
    {
        public int      ArtNo   { get; set; }
        public String   Name    { get; set; }
        public float    Price   { get; set; }
        public String   Type    { get; set; }
        public int      InStock { get; set; }
        public DateTime Date    { get; set; }

        public Product() { }

        public Product(int artNo, String name, float price, String type, int inStock){
            ArtNo = artNo;
            Name = name;
            Price = price;
            Type = type;
            InStock = inStock;
        }

        public Product(int artNo, String name, float price, String type, int inStock, DateTime date)
        {
            ArtNo = artNo;
            Name = name;
            Price = price;
            Type = type;
            InStock = inStock;
            Date = date.Date;
        }

        public Product(String artNo, String name, String price, String type, String inStock)
        {
            ArtNo = int.Parse(artNo);
            Name = name;
            if (price == "")
                Price = 0;
            else
                Price = float.Parse(price);
            Type = type;
            if (inStock == "")
                InStock = 0;
            else
                InStock = int.Parse(inStock);
        }

        public Product addProd(String artNo, String name, String price, String type, String inStock){
            ArtNo = int.Parse(artNo);
            Name = name;
            Price = float.Parse(price);
            Type = type;
            InStock = int.Parse(inStock);

            return this;
        }
    }
}
