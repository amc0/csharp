﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MediaShop
{
    class FileHandler
    {
        const String fileName = "stockfile.csv";
        const String fType = "csv";
        String expFileName = "stockexport.csv";
        const String exfType = "csv";
        StockHandler stockHandler;
        String exportPath;
        String expFile;
        BindingSource stock;
        StreamReader input;
        StreamWriter output;

        public FileHandler(StockHandler handler)
        {
            stockHandler = handler;
            stock = handler.StockSource;
        }

        public FileHandler(BindingSource stockSource)
        {
            stock = stockSource;
        }

        public void readFile(){

            if (!File.Exists(fileName))     //Om fil ej existerar så skapa den
                File.Create(fileName);

            input = new StreamReader(fileName);     //Läser in filinnehållet till buffert
            while (!input.EndOfStream)              //Går igenom rad för rad produktinformation lagrad i en sträng
            {
                String line = input.ReadLine();     //Hämtar nästa rad
                List<String> commaSep = line.Split(';').ToList<String>();       //Delar upp strängen baserad på ";"-som separerare
                if (commaSep.Count == 5)
                    stock.Add(new Product(commaSep[0], commaSep[1], commaSep[2], commaSep[3], commaSep[4]));    //Skapar en ny produkt baserat på sträng-separeringen
                else
                    MessageBox.Show("Korrupt objekt");              //Om sträng ej är enligt önskad formatering så anta att den är korrupt och hoppa till nästa

            }
            input.Close();
        }

        public void importFile()                                //Importerar fil vid klick av "import"
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = fType;
            openFile.Filter = "Text Files | *." + fType;
            DialogResult result = openFile.ShowDialog();
            if (openFile.FileName.Length > 0)
            {
                input = new StreamReader(openFile.FileName);     //Läser in filinnehållet till buffert
                while (!input.EndOfStream)              //Går igenom rad för rad produktinformation lagrad i en sträng
                {
                    String line = input.ReadLine();     //Hämtar nästa rad
                    List<String> commaSep = line.Split(';').ToList<String>();       //Delar upp strängen baserad på ";"-som separerare
                    if (commaSep.Count == 5 && (!stockHandler.find(commaSep[0].ToString())))                        //Kollar igenom önskad formatering samt kollar ifall artNr finns redan
                        stock.Add(new Product(commaSep[0], commaSep[1], commaSep[2], commaSep[3], commaSep[4]));    //Skapar en ny produkt baserat på sträng-separeringen
                }
                input.Close();
            }
            else if(result != DialogResult.Cancel) MessageBox.Show("Kunde inte läsa fil.", "Fel");
        }

        public void writeFile(BindingSource stockSource)            //Skriv lagerstatus till fil
        {
            if (!File.Exists(fileName))                     //Finns ej filen så skapa den
                output = File.AppendText(fileName);
            output = new StreamWriter(fileName);
            foreach (Product prod in stockSource)           //Itererar igenom samtliga produkter i lagret
            {
                output.WriteLine(prod.ArtNo + ";" + prod.Name + ";" + prod.Price + ";" + prod.Type + ";" + prod.InStock);   //Skriv ut produkt till fil
            }
            output.Close();
        }

        public void exportFile(BindingSource stockSource)            //Exporterar lagerstatus till fil
        {
            DialogResult result = DialogResult.OK;
            if (expFile==null||(!File.Exists(expFile))){                     //Finns ej filen så skapa den
                FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
                result = folderBrowser.ShowDialog();
                if (result != DialogResult.Cancel) { 
                    exportPath = folderBrowser.SelectedPath;
                    expFile = exportPath + "\\" + expFileName;
                    output = File.AppendText(expFile);
                }
            }
            if (result != DialogResult.Cancel) { 
                foreach (Product prod in stockSource)           //Itererar igenom samtliga produkter i lagret
                {
                    output.WriteLine(prod.ArtNo + ";" + prod.Name + ";" + prod.Price + ";" + prod.Type + ";" + prod.InStock);   //Skriv ut produkt till fil
                }
                output.Close();
            }
        }
    }
}
