﻿namespace MediaShop
{
    partial class NewProdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLbl = new System.Windows.Forms.Label();
            this.artNoLbl = new System.Windows.Forms.Label();
            this.typeLbl = new System.Windows.Forms.Label();
            this.priceLbl = new System.Windows.Forms.Label();
            this.inStockLbl = new System.Windows.Forms.Label();
            this.addBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.artNoBox = new System.Windows.Forms.TextBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.inStockBox = new System.Windows.Forms.TextBox();
            this.typeBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.Location = new System.Drawing.Point(13, 50);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(58, 17);
            this.nameLbl.TabIndex = 0;
            this.nameLbl.Text = "Namn: *";
            // 
            // artNoLbl
            // 
            this.artNoLbl.AutoSize = true;
            this.artNoLbl.Location = new System.Drawing.Point(13, 18);
            this.artNoLbl.Name = "artNoLbl";
            this.artNoLbl.Size = new System.Drawing.Size(62, 17);
            this.artNoLbl.TabIndex = 1;
            this.artNoLbl.Text = "Art. Nr: *";
            // 
            // typeLbl
            // 
            this.typeLbl.AutoSize = true;
            this.typeLbl.Location = new System.Drawing.Point(13, 83);
            this.typeLbl.Name = "typeLbl";
            this.typeLbl.Size = new System.Drawing.Size(45, 17);
            this.typeLbl.TabIndex = 2;
            this.typeLbl.Text = "Typ: *";
            // 
            // priceLbl
            // 
            this.priceLbl.AutoSize = true;
            this.priceLbl.Location = new System.Drawing.Point(13, 120);
            this.priceLbl.Name = "priceLbl";
            this.priceLbl.Size = new System.Drawing.Size(36, 17);
            this.priceLbl.TabIndex = 3;
            this.priceLbl.Text = "Pris:";
            // 
            // inStockLbl
            // 
            this.inStockLbl.AutoSize = true;
            this.inStockLbl.Location = new System.Drawing.Point(13, 156);
            this.inStockLbl.Name = "inStockLbl";
            this.inStockLbl.Size = new System.Drawing.Size(44, 17);
            this.inStockLbl.TabIndex = 4;
            this.inStockLbl.Text = "Antal:";
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(16, 208);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(75, 23);
            this.addBtn.TabIndex = 5;
            this.addBtn.Text = "Lägg Till";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(107, 208);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 6;
            this.cancelBtn.Text = "Avbryt";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // artNoBox
            // 
            this.artNoBox.Location = new System.Drawing.Point(82, 18);
            this.artNoBox.MaxLength = 12;
            this.artNoBox.Name = "artNoBox";
            this.artNoBox.Size = new System.Drawing.Size(100, 22);
            this.artNoBox.TabIndex = 0;
            this.artNoBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.artNoBox_KeyPress);
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(82, 50);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(100, 22);
            this.nameBox.TabIndex = 1;
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(82, 120);
            this.priceBox.MaxLength = 10;
            this.priceBox.Name = "priceBox";
            this.priceBox.Size = new System.Drawing.Size(100, 22);
            this.priceBox.TabIndex = 3;
            this.priceBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.priceBox_KeyPress);
            // 
            // inStockBox
            // 
            this.inStockBox.Location = new System.Drawing.Point(82, 156);
            this.inStockBox.MaxLength = 10;
            this.inStockBox.Name = "inStockBox";
            this.inStockBox.Size = new System.Drawing.Size(100, 22);
            this.inStockBox.TabIndex = 4;
            this.inStockBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inStockBox_KeyPress);
            // 
            // typeBox
            // 
            this.typeBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.typeBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.typeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeBox.FormattingEnabled = true;
            this.typeBox.Items.AddRange(new object[] {
            "Bok",
            "CD/DVD",
            "Ljudbok",
            "Spel"});
            this.typeBox.Location = new System.Drawing.Point(82, 83);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(100, 24);
            this.typeBox.TabIndex = 2;
            this.typeBox.SelectedIndexChanged += new System.EventHandler(this.typeBox_SelectedIndexChanged);
            // 
            // NewProdForm
            // 
            this.AcceptButton = this.addBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelBtn;
            this.ClientSize = new System.Drawing.Size(202, 253);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.inStockBox);
            this.Controls.Add(this.priceBox);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.artNoBox);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.inStockLbl);
            this.Controls.Add(this.priceLbl);
            this.Controls.Add(this.typeLbl);
            this.Controls.Add(this.artNoLbl);
            this.Controls.Add(this.nameLbl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewProdForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ny Produkt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label artNoLbl;
        private System.Windows.Forms.Label typeLbl;
        private System.Windows.Forms.Label priceLbl;
        private System.Windows.Forms.Label inStockLbl;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.TextBox artNoBox;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.TextBox inStockBox;
        private System.Windows.Forms.ComboBox typeBox;
    }
}