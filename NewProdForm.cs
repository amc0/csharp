﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MediaShop
{
    public partial class NewProdForm : Form
    {
        BindingSource products;
        StockHandler stock;

        public NewProdForm(StockHandler stockHandler)
        {
            InitializeComponent();
            stock = stockHandler;
            products = stockHandler.StockSource;
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            if (products.Count == 0 || stock.find(artNoBox.Text,nameBox.Text) == false)     //Om antalet i lager är 0 eller produkten ej existerar baserat på artNr och Name så lägg till produkt i lager
            {
                if (artNoBox.Text != "" && nameBox.Text != "" && typeBox.SelectedItem.ToString() != null)
                {
                    addProd();
                    this.Close();
                }
                else
                    MessageBox.Show("Fyll i alla obligatoriska fält märkta med *");         //Obligatorisk information saknas
            }
        }

        private void addProd()
        {
            products.Add(new Product(artNoBox.Text, nameBox.Text, priceBox.Text,            //Lägger till produkt till lager
                         typeBox.Text, inStockBox.Text));
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void allowOnlyNum(KeyPressEventArgs e)              //Tvingar fält att enbart ha numeriska tecken
        {
            if (Char.IsDigit(e.KeyChar)) { }                        //Om knapp nedtryckt är av typen Char så gör inget
            else if (e.KeyChar == '\b') { }                         //Om backspace är nedtryckt så gör inget
            else e.Handled = true;                                  //All annan inmatning anses som hanterad
        }

        private void allowNumDigit(KeyPressEventArgs e)             //Tvingar fält att enbart ha nummer eller decimaler.
        {
            base.OnKeyPress(e);
            NumberFormatInfo numberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
            string decimalSeparator = numberFormatInfo.NumberDecimalSeparator;      //Kodar in decimal-för sverige

            string keyInput = e.KeyChar.ToString();                                 //Gör om knapptryck till sträng
            if (Char.IsDigit(e.KeyChar)) { }                                        //Om knapp nedtryckt är nummer så gör inget
            else if(keyInput.Equals(decimalSeparator)) { }                          //Om knapp nedtryckt är decimaltecken så gör inget
            else if (e.KeyChar == '\b') { }                                         //Om backspace är nedtryckt så gör inget
            else e.Handled = true;                                                  //All annan inmatning anses som hanterad
        }

        private void artNoBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            allowOnlyNum(e);                                                        //Tillåter enbart nummer i fältet
        }

        private void typeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (typeBox.SelectedIndex == -1)
                MessageBox.Show("Värdet ej korrekt!");
        }

        private void artNoBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void priceBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            allowNumDigit(e);                                                     //Tillåter enbart nummer eller decimaltecken
        }

        private void inStockBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            allowOnlyNum(e);                                                    //Tillåter enbart nummer i fältet
        }

        
    }
}
