﻿namespace MediaShop
{
    partial class SellForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.sellPage = new System.Windows.Forms.TabPage();
            this.cartPanel = new System.Windows.Forms.Panel();
            this.pricePanel = new System.Windows.Forms.Panel();
            this.receiptPrintChkBox = new System.Windows.Forms.CheckBox();
            this.crncyLbl = new System.Windows.Forms.Label();
            this.chkOutBtn = new System.Windows.Forms.Button();
            this.totPriceLbl = new System.Windows.Forms.Label();
            this.priceLbl = new System.Windows.Forms.Label();
            this.chngAmtBtn = new System.Windows.Forms.Button();
            this.remCartBtn = new System.Windows.Forms.Button();
            this.emptCartBtn = new System.Windows.Forms.Button();
            this.cartGridView = new System.Windows.Forms.DataGridView();
            this.artNoDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.cartLbl = new System.Windows.Forms.Label();
            this.sellProdPanel = new System.Windows.Forms.Panel();
            this.sellProdGridView = new System.Windows.Forms.DataGridView();
            this.artCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inStockCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.sellBtnPanel = new System.Windows.Forms.Panel();
            this.repurBut = new System.Windows.Forms.Button();
            this.addToCartBtn = new System.Windows.Forms.Button();
            this.srchBtn = new System.Windows.Forms.Button();
            this.stockPage = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.prodGridView = new System.Windows.Forms.DataGridView();
            this.artNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inStockDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.stockBtnPanel = new System.Windows.Forms.Panel();
            this.setStckBtn = new System.Windows.Forms.Button();
            this.remProdBtn = new System.Windows.Forms.Button();
            this.newPrdBtn = new System.Windows.Forms.Button();
            this.statPage = new System.Windows.Forms.TabPage();
            this.top10Panel = new System.Windows.Forms.Panel();
            this.top10view = new System.Windows.Forms.DataGridView();
            this.artNoDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inStockDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.top10Lbl = new System.Windows.Forms.Label();
            this.prodSalesPanel = new System.Windows.Forms.Panel();
            this.prodComboBox = new System.Windows.Forms.ComboBox();
            this.monthComboBox = new System.Windows.Forms.ComboBox();
            this.yearComboBox = new System.Windows.Forms.ComboBox();
            this.prodSalesLbl = new System.Windows.Forms.Label();
            this.statGridView = new System.Windows.Forms.DataGridView();
            this.artNoDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inStockDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inköpsdatum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.receiptDoc = new System.Drawing.Printing.PrintDocument();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.arkivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.exporteraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.sellPage.SuspendLayout();
            this.cartPanel.SuspendLayout();
            this.pricePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cartGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource4)).BeginInit();
            this.sellProdPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sellProdGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource3)).BeginInit();
            this.sellBtnPanel.SuspendLayout();
            this.stockPage.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prodGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource5)).BeginInit();
            this.stockBtnPanel.SuspendLayout();
            this.statPage.SuspendLayout();
            this.top10Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.top10view)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource2)).BeginInit();
            this.prodSalesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.sellPage);
            this.tabControl.Controls.Add(this.stockPage);
            this.tabControl.Controls.Add(this.statPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 28);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(800, 620);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            this.tabControl.TabIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // sellPage
            // 
            this.sellPage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.sellPage.Controls.Add(this.cartPanel);
            this.sellPage.Controls.Add(this.sellProdPanel);
            this.sellPage.Controls.Add(this.sellBtnPanel);
            this.sellPage.Location = new System.Drawing.Point(4, 25);
            this.sellPage.Name = "sellPage";
            this.sellPage.Padding = new System.Windows.Forms.Padding(3);
            this.sellPage.Size = new System.Drawing.Size(792, 591);
            this.sellPage.TabIndex = 0;
            this.sellPage.Text = "Sälja";
            // 
            // cartPanel
            // 
            this.cartPanel.Controls.Add(this.pricePanel);
            this.cartPanel.Controls.Add(this.cartGridView);
            this.cartPanel.Controls.Add(this.cartLbl);
            this.cartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cartPanel.Location = new System.Drawing.Point(3, 354);
            this.cartPanel.Name = "cartPanel";
            this.cartPanel.Size = new System.Drawing.Size(786, 234);
            this.cartPanel.TabIndex = 4;
            // 
            // pricePanel
            // 
            this.pricePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pricePanel.Controls.Add(this.receiptPrintChkBox);
            this.pricePanel.Controls.Add(this.crncyLbl);
            this.pricePanel.Controls.Add(this.chkOutBtn);
            this.pricePanel.Controls.Add(this.totPriceLbl);
            this.pricePanel.Controls.Add(this.priceLbl);
            this.pricePanel.Controls.Add(this.chngAmtBtn);
            this.pricePanel.Controls.Add(this.remCartBtn);
            this.pricePanel.Controls.Add(this.emptCartBtn);
            this.pricePanel.Location = new System.Drawing.Point(555, 31);
            this.pricePanel.Name = "pricePanel";
            this.pricePanel.Size = new System.Drawing.Size(228, 198);
            this.pricePanel.TabIndex = 7;
            // 
            // receiptPrintChkBox
            // 
            this.receiptPrintChkBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.receiptPrintChkBox.AutoSize = true;
            this.receiptPrintChkBox.Location = new System.Drawing.Point(7, 173);
            this.receiptPrintChkBox.Name = "receiptPrintChkBox";
            this.receiptPrintChkBox.Size = new System.Drawing.Size(107, 21);
            this.receiptPrintChkBox.TabIndex = 1;
            this.receiptPrintChkBox.Text = "Kvittoutskrift";
            this.receiptPrintChkBox.UseVisualStyleBackColor = true;
            // 
            // crncyLbl
            // 
            this.crncyLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crncyLbl.AutoSize = true;
            this.crncyLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crncyLbl.ForeColor = System.Drawing.Color.Red;
            this.crncyLbl.Location = new System.Drawing.Point(188, 80);
            this.crncyLbl.Name = "crncyLbl";
            this.crncyLbl.Size = new System.Drawing.Size(37, 38);
            this.crncyLbl.TabIndex = 1;
            this.crncyLbl.Text = ":-";
            // 
            // chkOutBtn
            // 
            this.chkOutBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkOutBtn.Location = new System.Drawing.Point(118, 143);
            this.chkOutBtn.Name = "chkOutBtn";
            this.chkOutBtn.Size = new System.Drawing.Size(107, 51);
            this.chkOutBtn.TabIndex = 0;
            this.chkOutBtn.Text = "Slutför Köp";
            this.chkOutBtn.UseVisualStyleBackColor = true;
            this.chkOutBtn.Click += new System.EventHandler(this.chkOutBtn_Click);
            // 
            // totPriceLbl
            // 
            this.totPriceLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.totPriceLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totPriceLbl.ForeColor = System.Drawing.Color.Red;
            this.totPriceLbl.Location = new System.Drawing.Point(7, 80);
            this.totPriceLbl.Name = "totPriceLbl";
            this.totPriceLbl.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.totPriceLbl.Size = new System.Drawing.Size(192, 39);
            this.totPriceLbl.TabIndex = 2;
            this.totPriceLbl.Text = "0";
            // 
            // priceLbl
            // 
            this.priceLbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.priceLbl.AutoSize = true;
            this.priceLbl.Location = new System.Drawing.Point(4, 63);
            this.priceLbl.Name = "priceLbl";
            this.priceLbl.Size = new System.Drawing.Size(44, 17);
            this.priceLbl.TabIndex = 0;
            this.priceLbl.Text = "Total:";
            // 
            // chngAmtBtn
            // 
            this.chngAmtBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chngAmtBtn.Location = new System.Drawing.Point(7, 3);
            this.chngAmtBtn.Name = "chngAmtBtn";
            this.chngAmtBtn.Size = new System.Drawing.Size(107, 24);
            this.chngAmtBtn.TabIndex = 2;
            this.chngAmtBtn.Text = "Ändra Antal";
            this.chngAmtBtn.UseVisualStyleBackColor = true;
            this.chngAmtBtn.Click += new System.EventHandler(this.chngAmtBtn_Click);
            // 
            // remCartBtn
            // 
            this.remCartBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.remCartBtn.Location = new System.Drawing.Point(7, 33);
            this.remCartBtn.Name = "remCartBtn";
            this.remCartBtn.Size = new System.Drawing.Size(107, 24);
            this.remCartBtn.TabIndex = 3;
            this.remCartBtn.Text = "Ta Bort";
            this.remCartBtn.UseVisualStyleBackColor = true;
            this.remCartBtn.Click += new System.EventHandler(this.remCartBtn_Click);
            // 
            // emptCartBtn
            // 
            this.emptCartBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.emptCartBtn.Location = new System.Drawing.Point(118, 3);
            this.emptCartBtn.Name = "emptCartBtn";
            this.emptCartBtn.Size = new System.Drawing.Size(107, 54);
            this.emptCartBtn.TabIndex = 4;
            this.emptCartBtn.Text = "Töm Kundvagn";
            this.emptCartBtn.UseVisualStyleBackColor = true;
            this.emptCartBtn.Click += new System.EventHandler(this.emptCartBtn_Click);
            // 
            // cartGridView
            // 
            this.cartGridView.AllowUserToAddRows = false;
            this.cartGridView.AllowUserToDeleteRows = false;
            this.cartGridView.AllowUserToResizeRows = false;
            this.cartGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cartGridView.AutoGenerateColumns = false;
            this.cartGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cartGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.cartGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.cartGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.artNoDataGridViewTextBoxColumn4,
            this.nameDataGridViewTextBoxColumn4,
            this.Price,
            this.Amount});
            this.cartGridView.DataSource = this.productBindingSource4;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.cartGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.cartGridView.Location = new System.Drawing.Point(0, 31);
            this.cartGridView.MultiSelect = false;
            this.cartGridView.Name = "cartGridView";
            this.cartGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.cartGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.cartGridView.RowHeadersVisible = false;
            this.cartGridView.RowTemplate.Height = 24;
            this.cartGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.cartGridView.Size = new System.Drawing.Size(537, 198);
            this.cartGridView.TabIndex = 6;
            // 
            // artNoDataGridViewTextBoxColumn4
            // 
            this.artNoDataGridViewTextBoxColumn4.DataPropertyName = "ArtNo";
            this.artNoDataGridViewTextBoxColumn4.HeaderText = "Art.Nr";
            this.artNoDataGridViewTextBoxColumn4.Name = "artNoDataGridViewTextBoxColumn4";
            this.artNoDataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn4
            // 
            this.nameDataGridViewTextBoxColumn4.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn4.HeaderText = "Namn";
            this.nameDataGridViewTextBoxColumn4.Name = "nameDataGridViewTextBoxColumn4";
            this.nameDataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.HeaderText = "Pris";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.DataPropertyName = "InStock";
            this.Amount.HeaderText = "Antal";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // productBindingSource4
            // 
            this.productBindingSource4.DataSource = typeof(MediaShop.Product);
            // 
            // cartLbl
            // 
            this.cartLbl.AutoSize = true;
            this.cartLbl.Location = new System.Drawing.Point(3, 11);
            this.cartLbl.Name = "cartLbl";
            this.cartLbl.Size = new System.Drawing.Size(76, 17);
            this.cartLbl.TabIndex = 5;
            this.cartLbl.Text = "Kundvagn:";
            // 
            // sellProdPanel
            // 
            this.sellProdPanel.Controls.Add(this.sellProdGridView);
            this.sellProdPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.sellProdPanel.Location = new System.Drawing.Point(3, 60);
            this.sellProdPanel.Name = "sellProdPanel";
            this.sellProdPanel.Size = new System.Drawing.Size(786, 294);
            this.sellProdPanel.TabIndex = 2;
            // 
            // sellProdGridView
            // 
            this.sellProdGridView.AllowUserToAddRows = false;
            this.sellProdGridView.AllowUserToDeleteRows = false;
            this.sellProdGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.sellProdGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.sellProdGridView.AutoGenerateColumns = false;
            this.sellProdGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sellProdGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sellProdGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.sellProdGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sellProdGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.artCol,
            this.nameCol,
            this.priceCol,
            this.typeCol,
            this.inStockCol});
            this.sellProdGridView.DataSource = this.productBindingSource3;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sellProdGridView.DefaultCellStyle = dataGridViewCellStyle6;
            this.sellProdGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sellProdGridView.Location = new System.Drawing.Point(0, 0);
            this.sellProdGridView.MultiSelect = false;
            this.sellProdGridView.Name = "sellProdGridView";
            this.sellProdGridView.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sellProdGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.sellProdGridView.RowHeadersVisible = false;
            this.sellProdGridView.RowTemplate.Height = 24;
            this.sellProdGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sellProdGridView.Size = new System.Drawing.Size(786, 294);
            this.sellProdGridView.TabIndex = 0;
            this.sellProdGridView.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sellProdGridView_CellContentDoubleClick);
            // 
            // artCol
            // 
            this.artCol.DataPropertyName = "ArtNo";
            this.artCol.HeaderText = "Art.Nr";
            this.artCol.Name = "artCol";
            this.artCol.ReadOnly = true;
            this.artCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // nameCol
            // 
            this.nameCol.DataPropertyName = "Name";
            this.nameCol.HeaderText = "Namn";
            this.nameCol.Name = "nameCol";
            this.nameCol.ReadOnly = true;
            this.nameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // priceCol
            // 
            this.priceCol.DataPropertyName = "Price";
            this.priceCol.HeaderText = "Pris";
            this.priceCol.Name = "priceCol";
            this.priceCol.ReadOnly = true;
            // 
            // typeCol
            // 
            this.typeCol.DataPropertyName = "Type";
            this.typeCol.HeaderText = "Typ";
            this.typeCol.Name = "typeCol";
            this.typeCol.ReadOnly = true;
            // 
            // inStockCol
            // 
            this.inStockCol.DataPropertyName = "InStock";
            this.inStockCol.HeaderText = "Antal på lager";
            this.inStockCol.Name = "inStockCol";
            this.inStockCol.ReadOnly = true;
            // 
            // productBindingSource3
            // 
            this.productBindingSource3.DataSource = typeof(MediaShop.Product);
            // 
            // sellBtnPanel
            // 
            this.sellBtnPanel.Controls.Add(this.repurBut);
            this.sellBtnPanel.Controls.Add(this.addToCartBtn);
            this.sellBtnPanel.Controls.Add(this.srchBtn);
            this.sellBtnPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.sellBtnPanel.Location = new System.Drawing.Point(3, 3);
            this.sellBtnPanel.Name = "sellBtnPanel";
            this.sellBtnPanel.Size = new System.Drawing.Size(786, 57);
            this.sellBtnPanel.TabIndex = 3;
            // 
            // repurBut
            // 
            this.repurBut.Location = new System.Drawing.Point(142, 3);
            this.repurBut.Name = "repurBut";
            this.repurBut.Size = new System.Drawing.Size(107, 51);
            this.repurBut.TabIndex = 2;
            this.repurBut.Text = "Utför Återköp";
            this.repurBut.UseVisualStyleBackColor = true;
            this.repurBut.Click += new System.EventHandler(this.repurBut_Click);
            // 
            // addToCartBtn
            // 
            this.addToCartBtn.Location = new System.Drawing.Point(19, 3);
            this.addToCartBtn.Name = "addToCartBtn";
            this.addToCartBtn.Size = new System.Drawing.Size(107, 51);
            this.addToCartBtn.TabIndex = 1;
            this.addToCartBtn.Text = "Lägg till i Kundvagn";
            this.addToCartBtn.UseVisualStyleBackColor = true;
            this.addToCartBtn.Click += new System.EventHandler(this.addToCartBtn_Click);
            // 
            // srchBtn
            // 
            this.srchBtn.Location = new System.Drawing.Point(266, 3);
            this.srchBtn.Name = "srchBtn";
            this.srchBtn.Size = new System.Drawing.Size(107, 51);
            this.srchBtn.TabIndex = 0;
            this.srchBtn.Text = "Sök";
            this.srchBtn.UseVisualStyleBackColor = true;
            this.srchBtn.Click += new System.EventHandler(this.srchBtn_Click);
            // 
            // stockPage
            // 
            this.stockPage.Controls.Add(this.panel1);
            this.stockPage.Controls.Add(this.stockBtnPanel);
            this.stockPage.Location = new System.Drawing.Point(4, 25);
            this.stockPage.Name = "stockPage";
            this.stockPage.Padding = new System.Windows.Forms.Padding(3);
            this.stockPage.Size = new System.Drawing.Size(792, 591);
            this.stockPage.TabIndex = 1;
            this.stockPage.Text = "Lager";
            this.stockPage.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.prodGridView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(786, 528);
            this.panel1.TabIndex = 1;
            // 
            // prodGridView
            // 
            this.prodGridView.AllowUserToAddRows = false;
            this.prodGridView.AllowUserToDeleteRows = false;
            this.prodGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.prodGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.prodGridView.AutoGenerateColumns = false;
            this.prodGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.prodGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.prodGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.prodGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.prodGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.artNoDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.priceDataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.inStockDataGridViewTextBoxColumn});
            this.prodGridView.DataSource = this.productBindingSource5;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.prodGridView.DefaultCellStyle = dataGridViewCellStyle10;
            this.prodGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prodGridView.Location = new System.Drawing.Point(0, 0);
            this.prodGridView.MultiSelect = false;
            this.prodGridView.Name = "prodGridView";
            this.prodGridView.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.prodGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.prodGridView.RowHeadersVisible = false;
            this.prodGridView.RowTemplate.Height = 24;
            this.prodGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.prodGridView.Size = new System.Drawing.Size(786, 528);
            this.prodGridView.TabIndex = 0;
            // 
            // artNoDataGridViewTextBoxColumn
            // 
            this.artNoDataGridViewTextBoxColumn.DataPropertyName = "ArtNo";
            this.artNoDataGridViewTextBoxColumn.HeaderText = "Art.Nr";
            this.artNoDataGridViewTextBoxColumn.Name = "artNoDataGridViewTextBoxColumn";
            this.artNoDataGridViewTextBoxColumn.ReadOnly = true;
            this.artNoDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Namn";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // priceDataGridViewTextBoxColumn
            // 
            this.priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn.HeaderText = "Pris";
            this.priceDataGridViewTextBoxColumn.Name = "priceDataGridViewTextBoxColumn";
            this.priceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "Typ";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            this.typeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inStockDataGridViewTextBoxColumn
            // 
            this.inStockDataGridViewTextBoxColumn.DataPropertyName = "InStock";
            this.inStockDataGridViewTextBoxColumn.HeaderText = "Antal på lager";
            this.inStockDataGridViewTextBoxColumn.Name = "inStockDataGridViewTextBoxColumn";
            this.inStockDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // productBindingSource5
            // 
            this.productBindingSource5.DataSource = typeof(MediaShop.Product);
            // 
            // stockBtnPanel
            // 
            this.stockBtnPanel.Controls.Add(this.setStckBtn);
            this.stockBtnPanel.Controls.Add(this.remProdBtn);
            this.stockBtnPanel.Controls.Add(this.newPrdBtn);
            this.stockBtnPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.stockBtnPanel.Location = new System.Drawing.Point(3, 3);
            this.stockBtnPanel.Name = "stockBtnPanel";
            this.stockBtnPanel.Size = new System.Drawing.Size(786, 57);
            this.stockBtnPanel.TabIndex = 2;
            // 
            // setStckBtn
            // 
            this.setStckBtn.Location = new System.Drawing.Point(266, 3);
            this.setStckBtn.Name = "setStckBtn";
            this.setStckBtn.Size = new System.Drawing.Size(107, 51);
            this.setStckBtn.TabIndex = 2;
            this.setStckBtn.Text = "Leverans från Grossist";
            this.setStckBtn.UseVisualStyleBackColor = true;
            this.setStckBtn.Click += new System.EventHandler(this.setStckBtn_Click);
            // 
            // remProdBtn
            // 
            this.remProdBtn.Location = new System.Drawing.Point(142, 3);
            this.remProdBtn.Name = "remProdBtn";
            this.remProdBtn.Size = new System.Drawing.Size(107, 51);
            this.remProdBtn.TabIndex = 1;
            this.remProdBtn.Text = "Ta Bort Produkt";
            this.remProdBtn.UseVisualStyleBackColor = true;
            this.remProdBtn.Click += new System.EventHandler(this.remProdBtn_Click);
            // 
            // newPrdBtn
            // 
            this.newPrdBtn.Location = new System.Drawing.Point(19, 3);
            this.newPrdBtn.Name = "newPrdBtn";
            this.newPrdBtn.Size = new System.Drawing.Size(107, 51);
            this.newPrdBtn.TabIndex = 0;
            this.newPrdBtn.Text = "Ny Produkt";
            this.newPrdBtn.UseVisualStyleBackColor = true;
            this.newPrdBtn.Click += new System.EventHandler(this.newPrdBtn_Click);
            // 
            // statPage
            // 
            this.statPage.Controls.Add(this.top10Panel);
            this.statPage.Controls.Add(this.prodSalesPanel);
            this.statPage.Location = new System.Drawing.Point(4, 25);
            this.statPage.Name = "statPage";
            this.statPage.Padding = new System.Windows.Forms.Padding(3);
            this.statPage.Size = new System.Drawing.Size(792, 591);
            this.statPage.TabIndex = 2;
            this.statPage.Text = "Statistik";
            this.statPage.UseVisualStyleBackColor = true;
            // 
            // top10Panel
            // 
            this.top10Panel.Controls.Add(this.top10view);
            this.top10Panel.Controls.Add(this.top10Lbl);
            this.top10Panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.top10Panel.Location = new System.Drawing.Point(3, 313);
            this.top10Panel.Name = "top10Panel";
            this.top10Panel.Size = new System.Drawing.Size(786, 275);
            this.top10Panel.TabIndex = 3;
            // 
            // top10view
            // 
            this.top10view.AllowUserToAddRows = false;
            this.top10view.AllowUserToDeleteRows = false;
            this.top10view.AllowUserToResizeRows = false;
            this.top10view.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.top10view.AutoGenerateColumns = false;
            this.top10view.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.top10view.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.top10view.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.artNoDataGridViewTextBoxColumn3,
            this.nameDataGridViewTextBoxColumn3,
            this.priceDataGridViewTextBoxColumn3,
            this.typeDataGridViewTextBoxColumn3,
            this.inStockDataGridViewTextBoxColumn3});
            this.top10view.DataSource = this.productBindingSource2;
            this.top10view.Location = new System.Drawing.Point(0, 28);
            this.top10view.Name = "top10view";
            this.top10view.ReadOnly = true;
            this.top10view.RowHeadersVisible = false;
            this.top10view.RowTemplate.Height = 24;
            this.top10view.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.top10view.Size = new System.Drawing.Size(786, 244);
            this.top10view.TabIndex = 6;
            // 
            // artNoDataGridViewTextBoxColumn3
            // 
            this.artNoDataGridViewTextBoxColumn3.DataPropertyName = "ArtNo";
            this.artNoDataGridViewTextBoxColumn3.HeaderText = "Art.Nr";
            this.artNoDataGridViewTextBoxColumn3.Name = "artNoDataGridViewTextBoxColumn3";
            this.artNoDataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn3
            // 
            this.nameDataGridViewTextBoxColumn3.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn3.HeaderText = "Namn";
            this.nameDataGridViewTextBoxColumn3.Name = "nameDataGridViewTextBoxColumn3";
            this.nameDataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn3
            // 
            this.priceDataGridViewTextBoxColumn3.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn3.HeaderText = "Pris";
            this.priceDataGridViewTextBoxColumn3.Name = "priceDataGridViewTextBoxColumn3";
            this.priceDataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn3
            // 
            this.typeDataGridViewTextBoxColumn3.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn3.HeaderText = "Typ";
            this.typeDataGridViewTextBoxColumn3.Name = "typeDataGridViewTextBoxColumn3";
            this.typeDataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // inStockDataGridViewTextBoxColumn3
            // 
            this.inStockDataGridViewTextBoxColumn3.DataPropertyName = "InStock";
            this.inStockDataGridViewTextBoxColumn3.HeaderText = "Antal";
            this.inStockDataGridViewTextBoxColumn3.Name = "inStockDataGridViewTextBoxColumn3";
            this.inStockDataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // productBindingSource2
            // 
            this.productBindingSource2.DataSource = typeof(MediaShop.Product);
            // 
            // top10Lbl
            // 
            this.top10Lbl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.top10Lbl.AutoSize = true;
            this.top10Lbl.Location = new System.Drawing.Point(6, 8);
            this.top10Lbl.Name = "top10Lbl";
            this.top10Lbl.Size = new System.Drawing.Size(119, 17);
            this.top10Lbl.TabIndex = 5;
            this.top10Lbl.Text = "Top-10 produkter";
            // 
            // prodSalesPanel
            // 
            this.prodSalesPanel.Controls.Add(this.prodComboBox);
            this.prodSalesPanel.Controls.Add(this.monthComboBox);
            this.prodSalesPanel.Controls.Add(this.yearComboBox);
            this.prodSalesPanel.Controls.Add(this.prodSalesLbl);
            this.prodSalesPanel.Controls.Add(this.statGridView);
            this.prodSalesPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.prodSalesPanel.Location = new System.Drawing.Point(3, 3);
            this.prodSalesPanel.Name = "prodSalesPanel";
            this.prodSalesPanel.Size = new System.Drawing.Size(786, 307);
            this.prodSalesPanel.TabIndex = 2;
            // 
            // prodComboBox
            // 
            this.prodComboBox.FormattingEnabled = true;
            this.prodComboBox.Location = new System.Drawing.Point(146, 4);
            this.prodComboBox.Name = "prodComboBox";
            this.prodComboBox.Size = new System.Drawing.Size(121, 24);
            this.prodComboBox.TabIndex = 5;
            this.prodComboBox.Text = "Produkt";
            this.prodComboBox.SelectedIndexChanged += new System.EventHandler(this.prodComboBox_SelectedIndexChanged);
            // 
            // monthComboBox
            // 
            this.monthComboBox.FormattingEnabled = true;
            this.monthComboBox.Location = new System.Drawing.Point(400, 4);
            this.monthComboBox.Name = "monthComboBox";
            this.monthComboBox.Size = new System.Drawing.Size(121, 24);
            this.monthComboBox.TabIndex = 4;
            this.monthComboBox.Text = "Månad";
            this.monthComboBox.SelectedIndexChanged += new System.EventHandler(this.monthComboBox_SelectedIndexChanged);
            // 
            // yearComboBox
            // 
            this.yearComboBox.FormattingEnabled = true;
            this.yearComboBox.Location = new System.Drawing.Point(273, 4);
            this.yearComboBox.Name = "yearComboBox";
            this.yearComboBox.Size = new System.Drawing.Size(121, 24);
            this.yearComboBox.TabIndex = 3;
            this.yearComboBox.Text = "År";
            this.yearComboBox.SelectedIndexChanged += new System.EventHandler(this.yearComboBox_SelectedIndexChanged);
            // 
            // prodSalesLbl
            // 
            this.prodSalesLbl.AutoSize = true;
            this.prodSalesLbl.Location = new System.Drawing.Point(6, 4);
            this.prodSalesLbl.Name = "prodSalesLbl";
            this.prodSalesLbl.Size = new System.Drawing.Size(122, 17);
            this.prodSalesLbl.TabIndex = 2;
            this.prodSalesLbl.Text = "Produktförsäljning";
            // 
            // statGridView
            // 
            this.statGridView.AllowUserToAddRows = false;
            this.statGridView.AllowUserToDeleteRows = false;
            this.statGridView.AllowUserToResizeRows = false;
            this.statGridView.AutoGenerateColumns = false;
            this.statGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.statGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.statGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.statGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.artNoDataGridViewTextBoxColumn2,
            this.nameDataGridViewTextBoxColumn2,
            this.priceDataGridViewTextBoxColumn2,
            this.typeDataGridViewTextBoxColumn2,
            this.inStockDataGridViewTextBoxColumn2,
            this.Inköpsdatum});
            this.statGridView.DataSource = this.productBindingSource;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.statGridView.DefaultCellStyle = dataGridViewCellStyle13;
            this.statGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statGridView.Location = new System.Drawing.Point(0, 34);
            this.statGridView.Name = "statGridView";
            this.statGridView.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.statGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.statGridView.RowHeadersVisible = false;
            this.statGridView.RowTemplate.Height = 24;
            this.statGridView.Size = new System.Drawing.Size(786, 273);
            this.statGridView.TabIndex = 1;
            // 
            // artNoDataGridViewTextBoxColumn2
            // 
            this.artNoDataGridViewTextBoxColumn2.DataPropertyName = "ArtNo";
            this.artNoDataGridViewTextBoxColumn2.HeaderText = "Art.Nr";
            this.artNoDataGridViewTextBoxColumn2.Name = "artNoDataGridViewTextBoxColumn2";
            this.artNoDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn2
            // 
            this.nameDataGridViewTextBoxColumn2.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn2.HeaderText = "Namn";
            this.nameDataGridViewTextBoxColumn2.Name = "nameDataGridViewTextBoxColumn2";
            this.nameDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // priceDataGridViewTextBoxColumn2
            // 
            this.priceDataGridViewTextBoxColumn2.DataPropertyName = "Price";
            this.priceDataGridViewTextBoxColumn2.HeaderText = "Pris";
            this.priceDataGridViewTextBoxColumn2.Name = "priceDataGridViewTextBoxColumn2";
            this.priceDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // typeDataGridViewTextBoxColumn2
            // 
            this.typeDataGridViewTextBoxColumn2.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn2.HeaderText = "Typ";
            this.typeDataGridViewTextBoxColumn2.Name = "typeDataGridViewTextBoxColumn2";
            this.typeDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // inStockDataGridViewTextBoxColumn2
            // 
            this.inStockDataGridViewTextBoxColumn2.DataPropertyName = "InStock";
            this.inStockDataGridViewTextBoxColumn2.HeaderText = "Antal";
            this.inStockDataGridViewTextBoxColumn2.Name = "inStockDataGridViewTextBoxColumn2";
            this.inStockDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // Inköpsdatum
            // 
            this.Inköpsdatum.DataPropertyName = "Date";
            this.Inköpsdatum.HeaderText = "Inköpsdatum";
            this.Inköpsdatum.Name = "Inköpsdatum";
            this.Inköpsdatum.ReadOnly = true;
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataSource = typeof(MediaShop.Product);
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Antal";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "á Pris";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Typ";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Namn";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Art. Nr";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arkivToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // arkivToolStripMenuItem
            // 
            this.arkivToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.arkivToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exporteraToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.arkivToolStripMenuItem.Name = "arkivToolStripMenuItem";
            this.arkivToolStripMenuItem.Size = new System.Drawing.Size(54, 24);
            this.arkivToolStripMenuItem.Text = "Arkiv";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.importToolStripMenuItem.Text = "Importera";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.exitToolStripMenuItem.Text = "Avsluta";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // productBindingSource1
            // 
            this.productBindingSource1.DataSource = typeof(MediaShop.Product);
            // 
            // exporteraToolStripMenuItem
            // 
            this.exporteraToolStripMenuItem.Name = "exporteraToolStripMenuItem";
            this.exporteraToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.exporteraToolStripMenuItem.Text = "Exportera lager";
            this.exporteraToolStripMenuItem.Click += new System.EventHandler(this.exporteraToolStripMenuItem_Click);
            // 
            // SellForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 648);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(430, 670);
            this.Name = "SellForm";
            this.ShowIcon = false;
            this.Text = "Media Shop";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SellForm_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.sellPage.ResumeLayout(false);
            this.cartPanel.ResumeLayout(false);
            this.cartPanel.PerformLayout();
            this.pricePanel.ResumeLayout(false);
            this.pricePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cartGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource4)).EndInit();
            this.sellProdPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sellProdGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource3)).EndInit();
            this.sellBtnPanel.ResumeLayout(false);
            this.stockPage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prodGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource5)).EndInit();
            this.stockBtnPanel.ResumeLayout(false);
            this.statPage.ResumeLayout(false);
            this.top10Panel.ResumeLayout(false);
            this.top10Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.top10view)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource2)).EndInit();
            this.prodSalesPanel.ResumeLayout(false);
            this.prodSalesPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage sellPage;
        private System.Windows.Forms.TabPage stockPage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView prodGridView;
        private System.Windows.Forms.Panel stockBtnPanel;
        private System.Windows.Forms.Button setStckBtn;
        private System.Windows.Forms.Button remProdBtn;
        private System.Windows.Forms.Button newPrdBtn;
        private System.Windows.Forms.Panel sellProdPanel;
        private System.Windows.Forms.DataGridView sellProdGridView;
        private System.Windows.Forms.Panel sellBtnPanel;
        private System.Windows.Forms.Button chngAmtBtn;
        private System.Windows.Forms.Button addToCartBtn;
        private System.Windows.Forms.Button srchBtn;
        private System.Windows.Forms.Button chkOutBtn;
        private System.Windows.Forms.Panel cartPanel;
        private System.Windows.Forms.Button emptCartBtn;
        private System.Windows.Forms.Button remCartBtn;
        private System.Windows.Forms.Label cartLbl;
        private System.Windows.Forms.DataGridView cartGridView;
        private System.Windows.Forms.Panel pricePanel;
        private System.Windows.Forms.Label crncyLbl;
        private System.Windows.Forms.Label totPriceLbl;
        private System.Windows.Forms.Label priceLbl;
        private System.Windows.Forms.CheckBox receiptPrintChkBox;
        private System.Windows.Forms.Button repurBut;
        private System.Drawing.Printing.PrintDocument receiptDoc;
        private System.Windows.Forms.TabPage statPage;
        private System.Windows.Forms.DataGridView statGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Panel top10Panel;
        private System.Windows.Forms.Panel prodSalesPanel;
        private System.Windows.Forms.BindingSource productBindingSource;
        private System.Windows.Forms.Label prodSalesLbl;
        private System.Windows.Forms.DataGridView top10view;
        private System.Windows.Forms.BindingSource productBindingSource1;
        private System.Windows.Forms.Label top10Lbl;
        private System.Windows.Forms.ComboBox monthComboBox;
        private System.Windows.Forms.ComboBox yearComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn artNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inStockDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn artNoDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn inStockDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn artNoDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.BindingSource productBindingSource2;
        private System.Windows.Forms.BindingSource productBindingSource4;
        private System.Windows.Forms.BindingSource productBindingSource3;
        private System.Windows.Forms.BindingSource productBindingSource5;
        private System.Windows.Forms.DataGridViewTextBoxColumn artNoDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn inStockDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inköpsdatum;
        private System.Windows.Forms.DataGridViewTextBoxColumn artCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn inStockCol;
        private System.Windows.Forms.ComboBox prodComboBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem arkivToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exporteraToolStripMenuItem;
    }
}

