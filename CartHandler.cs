﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MediaShop
{
    class CartHandler
    {
        BindingSource cartSource;
        float totPrice;

        public CartHandler()
        {
            cartSource = new BindingSource();
            cartSource.ListChanged += priceUpdate;      //Eventhandler för när listan ändras så skall priset justeras
        }

        public BindingSource CartSource { get { return cartSource; } }
        public String TotPrice { get { return totPrice.ToString(); } }

        private void priceUpdate(object sender, EventArgs e)
        {
            setTotPrice();                          //Uppdaterar totalpris för produkter i kundvagn
        }

        public void setTotPrice()                   //Går igenom samtliga produkter i kundvagn för att generera totalpris
        {
            totPrice = 0;
            if(cartSource.Count>0){
                foreach (Product prod in cartSource)
                {
                    int amt = prod.InStock;
                    float price = prod.Price;
                    totPrice += (amt * price);
                }
            }
            
        }

        public void add(Product product, int antal)             //Lägg till produkt i kundvagn
        {
            Product cartProduct = new Product(product.ArtNo, product.Name, product.Price, product.Type, antal);
            cartSource.Add(cartProduct);
        }

        public void remove(Product product)                     //Ta bort produkt från kundvagn
        {
            if (find(product)!=null)                            //Letar upp produkten och finns den i listan så tar den bort produkten
                cartSource.Remove(find(product));
        }

        public void remove()
        {
            cartSource.RemoveCurrent();                         //Tar bort markerad produkt i kundvagn från kundvagnen
        }

        public void setInCart(int antal)                        //Ändra antalet produkter man vill köpa i kundvagnen
        {
            Product product = (Product)cartSource.Current;
            product.InStock = antal;
            update();
        }

        public void clear()                                     //Tömmer kundvagn
        {
            cartSource.Clear();
        }

        public void update()                                    //Uppdaterar kundvagn så att ändringar av produkter slår igenom
        {
            cartSource.ResetBindings(false);
        }

        public bool contains(Product product)                   //Kollar ifall kundvagnen redan innehåller en produkt
        {
            if (find(product) != null) return true;
            else return false;
        }

        public Product find(Product product)                    //Letar upp en produkt och returnerar den ifall den finns i kundvagn
        {
            foreach (Product cartProd in cartSource)
            {
                if (product.ArtNo == cartProd.ArtNo)
                    return cartProd;
            }
            return null;
        }
    }
}
