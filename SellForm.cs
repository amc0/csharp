﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows.Forms;

namespace MediaShop
{
    public partial class SellForm : Form
    {
        const int ArtNoCell = 0;
        const int CartAmtCell = 2;
        const int AmtToBuyCell = 3;
        const int LagerCell = 4;
        const int PriceCell = 3;

        StockHandler saveStock;
        CartHandler cartHandler;
        FileHandler fileHandler;
        StatHandler statHandler;
        StockHandler stockHandler;

        public SellForm()
        {
            InitializeComponent();
            stockHandler = new StockHandler();
            cartHandler = new CartHandler();
            fileHandler = new FileHandler(stockHandler);
            fileHandler.readFile();
            statHandler = new StatHandler();
            prodGridView.DataSource = stockHandler.StockSource;
            sellProdGridView.DataSource = stockHandler.StockSource;
            cartGridView.DataSource = cartHandler.CartSource;
            top10view.DataSource = statHandler.Top10Source;
            statGridView.DataSource = statHandler.StatSource;
            updateComboBox();                                       //Initialiserar comboBoxarna i Statistik-delen av programmet.
        }

        private void srchBtn_Click(object sender, EventArgs e)
        {
            if (srchBtn.Text == "Sök") {
                saveStock = new StockHandler(stockHandler.StockSource);             //Sparar undand lager-BindingSource så man lätt kan byta tillbaka
                SearchForm srch = new SearchForm(stockHandler.StockSource);         //Skapar ny SearchForm
                DialogResult dRes = srch.ShowDialog();
                if(dRes != DialogResult.Cancel)
                {
                    stockHandler = new StockHandler(srch.Stock);                    //Skapar ny bindingSource baserad på vad sökningen genererade
                    sellProdGridView.DataSource = stockHandler.StockSource;         //Ändrar vyn så att man bara visar sökta produkter
                    srchBtn.Text = "Rensa sökning";                                 //Ändra knapp så att man kan återställa vyn till ursprungliga saveStock
                }
            }
            else {
                resetSearch();                                                      //Återställer produkt-vyn
            }
        }

        private void resetSearch()                                      //Återstället produktvyn så att den visar ursprungsprodukterna, dvs. rensar sökningen.
        {

            stockHandler = new StockHandler(saveStock.StockSource);     //Skapar ny stockHandler med ursprungs-bindingSource
            sellProdGridView.DataSource = stockHandler.StockSource;     //Ändrar produktvyn till ursprungliga
            srchBtn.Text = "Sök";                                       //Ändrar knapp så att man kan söka på nytt
        }

        private void addNewProd()                                       //Formulär för att lägga till en ny produkt i lagret.
        {
            NewProdForm newProd = new NewProdForm(stockHandler);
            newProd.Show();
        }

        private void newPrdBtn_Click(object sender, EventArgs e)
        {
            addNewProd();                                               //Dirigeras om till den generella addNewProd()
        }

        private void updatePrice()                                      //Uppdaterar pris-labeln i höger-nederkant.
        {
            totPriceLbl.Text = cartHandler.TotPrice;
        }
        
        private void addToCart()
        {
            if (sellProdGridView.CurrentCell != null)
            {
                int antalPaLager;
                int antal = -1;
                String input = Interaction.InputBox("Ange antal:", "Lägg till i kundvagn", "");     //Öppnar upp en interactionbox för input av antalet man vill lägga till i kundvagn
                if (input.Length > 0) antal = int.Parse(input);
                if (antal > 0)
                {
                    antalPaLager = stockHandler.checkStock();                                       //Kollar om nog många finns i lager
                    if (antal <= antalPaLager)
                    {
                        Product stockProd = stockHandler.StockSource.Current as Product;            //Markerar valt objekt i produktvyn
                        if (!cartHandler.contains(stockProd)) {                                     //Kollar om produkten redan finns i kundvagnen
                            cartHandler.add(stockProd, antal);                                      //Lägger till prdukten i kundvagn
                            updatePrice();                                                          //Uppdaterar pris för produkter i kundvagn
                        }
                        else
                        {
                            Product cartProd = cartHandler.find(stockProd);                         //Finns produkten så leta fram den i kundvagn-källan
                            if (stockHandler.checkStock() >= (cartProd.InStock + antal)) {          //Kollar om nog många antal finns baserat på antalet som redan finns i kundvagn + antalet man försöker lägga till
                                cartProd.InStock += antal;                                          //Plussa på antalet med de som redan finns i kundvagn för given produkt
                                cartHandler.update();                                               //Uppdatera källan så att antal-ändras
                            }
                            else
                                MessageBox.Show("Antal överskrider antalet som finns på lager.", "Fel");
                        }

                    }
                    else if (antal > antalPaLager)
                        MessageBox.Show("Antal måste vara mindre än vad som finns på lager.", "Fel");
                }
            }
        }

        private void addToCartBtn_Click(object sender, EventArgs e)
        {
            addToCart();                                                            //Dirigeras om till den generella addToCart()
        }

        private void remProd()                                                      //Ta bort produkt ur sortiment samt om den finns i kundvagn
        {
            Product product = (Product)stockHandler.StockSource.Current;            //Lagra markerad produkt
            cartHandler.remove(product);                                            //Ta bort från kundvagn
            stockHandler.remove(product);                                           //Ta bort från lager
        }

        private void remProdBtn_Click(object sender, EventArgs e)
        {
            if (stockHandler.checkStock()>0)                                        //Kräv en bekräftelse om antalet > 0
            {
                DialogResult dialogRes = MessageBox.Show("Vill du verkligen ta bort produkt?", "Bekräfta borttagning", MessageBoxButtons.YesNoCancel);
                switch (dialogRes)
                {
                    case DialogResult.Yes: remProd(); updatePrice(); break;
                    case DialogResult.No: break;
                    case DialogResult.Cancel: break;
                }
            }
            else
                remProd();                                                          //Om antalet == 0, ta bort utan bekräftelse
        }

        private void setInStock()                                                   //Ändra antal av produkt på lager
        {
            int antal = -1;
            String input = Interaction.InputBox("Ange antal:", "Ändra antal på lager", "");
            if (input.Length > 0) antal = int.Parse(input);
            if (antal > -1)
                stockHandler.setInStock(antal);                                     //Sätter antal på lager
        }

        private void setStckBtn_Click(object sender, EventArgs e)
        {
            if (prodGridView.RowCount > 0) {
                setInStock();                                                  //Dirigeras om till generella setInStock()
            }
            else {                                                              //Om listan är tom, fråga om att skapa ny produkt
                DialogResult dialogRes = MessageBox.Show("Listan är tom, lägga till ny produkt?","Listan tom",MessageBoxButtons.YesNo);
                if (dialogRes == DialogResult.Yes) addNewProd();
            }
        }

        private void setInCart()                            //Ändra antalet av varan man önskar köpa i kundvagn
        {
            int antal = -1;
            int.TryParse(Interaction.InputBox("Ange antal:", "Ändra antal att sälja", "", -1, -1), out antal);
            int antalPaLager = stockHandler.checkStock();   //Kollar lagerstatus så den inte överskrids
            if (antal > 0 && antal <= antalPaLager)
            {
                cartHandler.setInCart(antal);               //Ändrar antalet i kundvagnen av önskad produkt
                updatePrice();                              //Uppdatera priset
            }
            else if (antal == 0)
                remCartProd();                              //Sätter man antalet till 0 tas produkten bort från kundvagnen
            else
                MessageBox.Show("Ej nog antal på lager.", "Lagerstatus");
        }

        private void chngAmtBtn_Click(object sender, EventArgs e)
        {
            if (cartGridView.CurrentCell != null) {
                setInCart();                                //Dirigerar om till generella setInCart()
            }
        }

        private void remCartProd()                          //Tar bort produkt från kundvagn
        {
            if (cartGridView.CurrentCell != null)           //Koll så att en produkt är vald innan man försöker ta bort
            {
                cartHandler.remove();                       //Ta bort produkt ur kundvagn
                updatePrice();                              //Uppdatera pris-label
            }
        }

        private void remCartBtn_Click(object sender, EventArgs e)
        {
            remCartProd();                                  //Dirigera om till generella remCartProd()
        }

        private void emptCart()                             //Tömmer hela kundvagnen på produkter
        {
            cartHandler.clear();                            //Tömmer cartSource BindingSource på produkter
            updatePrice();                                  //Uppdaterar priset
        }

        private void emptCartBtn_Click(object sender, EventArgs e)
        {
            emptCart();                                     //Anropar generella emptCart()
        }

        private void updateComboBox()
        {
            yearComboBox.DataSource = statHandler.Years;        //Uppdaterar comboBox vid initialisering samt när ny produkt lagts till
            monthComboBox.DataSource = statHandler.Months;      //Uppdaterar comboBox vid initialisering samt när nytt år lagts till
            prodComboBox.DataSource = statHandler.Products;     //Uppdaterar comboBox vid initialisering samt när ny månad lagts till
        }

        private void checkOut()                                 //Slutför köp
        {
            if (cartHandler.CartSource.Count > 0) {             //Kollar så att en eller fler produkter finns i kundvagn
                foreach (Product prod in cartHandler.CartSource)    //Går igenom varje produkt i kundvagn
                {
                    int antal = prod.InStock;                       //Hämtar produktantalet som man lagt i kundvagn för önskad produkt
                    int artNo = prod.ArtNo;                         //Hämtar artNr för önskad produkt
                    chngStock(artNo, antal);                        //Ändrar antalet på lager för önskad produkt, dvs. -antalet man lagt i kundvagn
                }
                if (receiptPrintChkBox.Checked) {                   //Skriver ut kvitto ifall kvitto check-box är markerad
                    Receipt receipt = new Receipt(cartHandler.CartSource, cartHandler.TotPrice);
                }
                statHandler.Add(cartHandler.CartSource);            //Skapar ny statistikhanterare
                top10view.DataSource = statHandler.updateTop10();   //Uppdaterar top10produkter
                updateComboBox();                                   //Uppdaterar comboBox med nya värden av produkt, år, månad ifall de ej finns sen tidigare
                emptCart();                                         //Tömmer kundvagn inför nytt köp
            }
        }

        private void chkOutBtn_Click(object sender, EventArgs e)
        {
            checkOut();                                             //Dirigerar om till generella checkOut()
        }

        private void chngStock(int artNo, int ant)
        {
            stockHandler.setInStock(artNo, ant);                    //Ändrar antalet i lager baserat på artNr
        }

        private void repurBut_Click(object sender, EventArgs e)
        {
            stockHandler.repurchase();                              //Återköp av önskad vara, dvs. antalet i lager++
        }

        private void sellProdGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            addToCart();                                            //Dirigerar om till generella addToCart() OBS! Vid dubbelklickn av cell
        }

        private void SellForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            fileHandler.writeFile(stockHandler.StockSource);        //När applikation stängs så skrivs innehållet i lager till fil
        }

        private void statHandlerSearch()                            //Hanterar produktfiltrering i statistik-delen
        {
            if (prodComboBox.DataSource == null || yearComboBox.DataSource == null || monthComboBox.DataSource == null) { } //Gör inget om samtliga combobox är tomma
            else if (prodComboBox.Text != "-- Produkt --" && yearComboBox.Text != "-- År --" && monthComboBox.Text != "-- Månad --") //Genomför sökning om standardvärden i comboBox byts till annat
            {
                statGridView.DataSource = statHandler.comboBoxSearch(prodComboBox.Text, yearComboBox.Text, monthComboBox.Text);     //Skapar ny källa och ändrar vy till nya källan baserat på önskad filtrering
            }
            else
                statGridView.DataSource = statHandler.StatSource;                       //Ändrar tillbaka till ursprung om standardvärden är satta i comboBox
        }

        private void yearComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            statHandlerSearch();                                                        //Dirigerar om till generell statHandlerSearch
        }

        private void prodComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            statHandlerSearch();                                                        //Dirigerar om till generell statHandlerSearch
        }

        private void monthComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            statHandlerSearch();                                                        //Dirigerar om till generell statHandlerSearch
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (srchBtn.Text == "Rensa sökning")
            {
                resetSearch();                                                          //Ändras flik så skall befintlig sökning (om sådan är satt) återställas
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileHandler import = new FileHandler(stockHandler);
            import.importFile();
        }

        private void exporteraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fileHandler.exportFile(stockHandler.StockSource);
        }

    }
}
