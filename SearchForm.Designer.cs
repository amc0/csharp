﻿namespace MediaShop
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchTxtBox = new System.Windows.Forms.TextBox();
            this.searchPanel = new System.Windows.Forms.Panel();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.typeBtn = new System.Windows.Forms.RadioButton();
            this.nameBtn = new System.Windows.Forms.RadioButton();
            this.artNrBtn = new System.Windows.Forms.RadioButton();
            this.searchBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.searchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchTxtBox
            // 
            this.searchTxtBox.Location = new System.Drawing.Point(62, 50);
            this.searchTxtBox.Name = "searchTxtBox";
            this.searchTxtBox.Size = new System.Drawing.Size(175, 22);
            this.searchTxtBox.TabIndex = 0;
            // 
            // searchPanel
            // 
            this.searchPanel.Controls.Add(this.cancelBtn);
            this.searchPanel.Controls.Add(this.typeBtn);
            this.searchPanel.Controls.Add(this.nameBtn);
            this.searchPanel.Controls.Add(this.artNrBtn);
            this.searchPanel.Controls.Add(this.searchBtn);
            this.searchPanel.Controls.Add(this.label1);
            this.searchPanel.Controls.Add(this.label2);
            this.searchPanel.Controls.Add(this.searchTxtBox);
            this.searchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchPanel.Location = new System.Drawing.Point(0, 0);
            this.searchPanel.Name = "searchPanel";
            this.searchPanel.Size = new System.Drawing.Size(263, 128);
            this.searchPanel.TabIndex = 1;
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(157, 87);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(80, 23);
            this.cancelBtn.TabIndex = 8;
            this.cancelBtn.Text = "Avbryt";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // typeBtn
            // 
            this.typeBtn.AutoSize = true;
            this.typeBtn.Location = new System.Drawing.Point(184, 23);
            this.typeBtn.Name = "typeBtn";
            this.typeBtn.Size = new System.Drawing.Size(53, 21);
            this.typeBtn.TabIndex = 6;
            this.typeBtn.Text = "Typ";
            this.typeBtn.UseVisualStyleBackColor = true;
            // 
            // nameBtn
            // 
            this.nameBtn.AutoSize = true;
            this.nameBtn.Location = new System.Drawing.Point(112, 23);
            this.nameBtn.Name = "nameBtn";
            this.nameBtn.Size = new System.Drawing.Size(66, 21);
            this.nameBtn.TabIndex = 5;
            this.nameBtn.Text = "Namn";
            this.nameBtn.UseVisualStyleBackColor = true;
            // 
            // artNrBtn
            // 
            this.artNrBtn.AutoSize = true;
            this.artNrBtn.Checked = true;
            this.artNrBtn.Location = new System.Drawing.Point(36, 23);
            this.artNrBtn.Name = "artNrBtn";
            this.artNrBtn.Size = new System.Drawing.Size(66, 21);
            this.artNrBtn.TabIndex = 4;
            this.artNrBtn.TabStop = true;
            this.artNrBtn.Text = "Art.Nr";
            this.artNrBtn.UseVisualStyleBackColor = true;
            // 
            // searchBtn
            // 
            this.searchBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.searchBtn.Location = new System.Drawing.Point(62, 87);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(83, 23);
            this.searchBtn.TabIndex = 3;
            this.searchBtn.Text = "Sök";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Söker efter:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Text:";
            // 
            // SearchForm
            // 
            this.AcceptButton = this.searchBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelBtn;
            this.ClientSize = new System.Drawing.Size(263, 128);
            this.Controls.Add(this.searchPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sök";
            this.searchPanel.ResumeLayout(false);
            this.searchPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox searchTxtBox;
        private System.Windows.Forms.Panel searchPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.RadioButton typeBtn;
        private System.Windows.Forms.RadioButton nameBtn;
        private System.Windows.Forms.RadioButton artNrBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}