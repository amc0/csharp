﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MediaShop
{
    public partial class SearchForm : Form
    {
        BindingSource stock;
        SearchHandler searchHandler;
        public SearchForm(BindingSource stockSource)
        {
            searchHandler = new SearchHandler();
            InitializeComponent();
            stock = stockSource;
        }

        public BindingSource Stock{ get {return searchHandler.SearchSource;}}


        private void searchBtn_Click(object sender, EventArgs e)
        {
            searchHandler = new SearchHandler(stock);
            var checkedButton = searchPanel.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);
            switch (checkedButton.Text)
            {
                case "Art.Nr": searchHandler.searchArtNo(searchTxtBox.Text); break;
                case "Namn": searchHandler.searchName(searchTxtBox.Text); break;
                case "Typ": searchHandler.searchType(searchTxtBox.Text); break;
                default: break;
            }
            this.Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
