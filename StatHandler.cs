﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MediaShop
{
    class StatHandler
    {
        BindingSource statSource;
        BindingSource top10Source;
        BindingList<String> products;
        List<Product> top10;
        BindingList<String> years;
        BindingList<String> months;

        public StatHandler()
        {
            statSource = new BindingSource();
            top10Source = new BindingSource();
            products = new BindingList<String>();
            years = new BindingList<String>();
            months = new BindingList<String>();
            top10 = new List<Product>();
            top10Source.DataSource = top10;
            products.Add("-- Produkt --");
            years.Add("-- År --");
            months.Add("-- Månad --");
            statSource.ListChanged += updateDictionary;
        }

        public StatHandler(BindingSource stat)
        {
            statSource = stat;
        }

        public BindingSource           StatSource  { get { return statSource; } }
        public BindingSource           Top10Source { get { return top10Source; } }
        public BindingList<String>     Years       { get { return years; } }
        public BindingList<String>     Months      { get { return months; } }
        public BindingList<String>     Products    { get { return products; } }

        private Product find(int artNo, List<Product> source)       //Letar upp produkt i statistik-källa
        {
            Product returnProd = null;
            foreach (Product prod in source)
            {
                if (prod.ArtNo == artNo) { 
                    returnProd = prod;
                    break;
                }
                
            }

            return returnProd;
        }

        public void Add(BindingSource cartSource)           
        {
            foreach (Product prod in cartSource)
            {
                statSource.Add(new Product(prod.ArtNo, prod.Name, prod.Price, prod.Type, prod.InStock, DateTime.Now));
                updateTop10();
            }
        }

        public List<Product> updateTop10(){
            top10.Clear();
            foreach (Product prod in statSource)
            {
                Product item = find(prod.ArtNo, top10);
                if (item == null)
                    top10.Add(new Product(prod.ArtNo,prod.Name,prod.Price,prod.Type,prod.InStock));
                else if (item != null)
                    item.InStock += prod.InStock;   
            }
            top10 = top10.OrderByDescending(x => x.InStock).ToList();
            return top10;
        }

        public void updateDictionary(object sender, EventArgs e)
        {
            foreach (Product prod in statSource)
            {
                String[] prodYear = prod.Date.ToString().Split('-');
                if (!products.Contains(prod.Name))
                {
                    products.Add(prod.Name);
                }
                if (!years.Contains(prodYear[0])) { 
                    years.Add(prodYear[0]);
                }
                if(!months.Contains(prodYear[1]))
                    months.Add(prodYear[1]);
            }

        }

        public BindingSource comboBoxSearch(String product, String year, String month)
        {
            BindingSource searchSource = new BindingSource();
            foreach (Product prod in StatSource)
            {
                String[] prodDate = prod.Date.ToString().Split('-');
                if (prod.Name == product && prodDate[0] == year && prodDate[1] == month)
                {
                    searchSource.Add(prod);
                }
            }
            return searchSource;
        }


    }
}
