﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MediaShop
{
    class Receipt
    {
        BindingSource cart;
        List<string> items;
        float totalPrice;

        public Receipt(BindingSource cartSource, String totPrice)
        {
            PrintDocument receiptDoc = new PrintDocument();
            cart = cartSource;
            convRowsToString();
            float.TryParse(totPrice, out totalPrice);
            receiptDoc.PrintPage += receiptDoc_PrintPage;
            PrintDialog print = new PrintDialog();
            print.ShowDialog();
            receiptDoc.Print();
        }

        private void convRowsToString()                 //Konverterar kundvagn till sträng för utskrift
        {
            items = new List<string>(cart.Count);
            foreach (Product prod in cart)
            {
                var str = new StringBuilder();
                str.Append(prod.ArtNo + "\t" + prod.Name + "\t\t" + prod.Price + "\t\t" + prod.InStock);

                items.Add(str.ToString());
            }
        }

        private void receiptDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)      //Handler för printning
        {
            int row = 0;
            int indent = 15;

            e.Graphics.DrawString("Kvitto - Inköpsdatum: " + DateTime.Now.ToString("d"), SystemFonts.CaptionFont, Brushes.Black, 0, row);
            row += 20;
            e.Graphics.DrawString("Art. Nr\tNamn\t\tPris\t\tAntal", SystemFonts.DefaultFont, Brushes.Black, 0, row);
            row += 20;
            e.Graphics.DrawLine(new Pen(Brushes.Black), new Point(0, row), new Point(400, row));
            row += 10;

            foreach (String item in items)
            {
                e.Graphics.DrawString(item, SystemFonts.DefaultFont, Brushes.Black, 0, row);
                row += indent;
            }
            row += 10;
            e.Graphics.DrawLine(new Pen(Brushes.Black), new Point(0, row), new Point(400, row));
            e.Graphics.DrawString("Totalpris: " + totalPrice, SystemFonts.CaptionFont, Brushes.Black, 0, row);

            }
    }
}
