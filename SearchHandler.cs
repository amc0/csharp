﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MediaShop
{
    
    class SearchHandler
    {
        BindingSource searchSource, stock;

        public SearchHandler()
        {
            searchSource = new BindingSource();
        }

        public BindingSource SearchSource { get { return searchSource; } }

        public SearchHandler(BindingSource stockSource)
        {
            searchSource = new BindingSource();
            stock = stockSource;
        }

        public void searchArtNo(String artNo)           //Letar upp önskad produkt baserat på artNr och lägger till i söknings-källa
        {
            foreach (Product prod in stock)
            {
                if (prod.ArtNo.ToString().Contains(artNo))
                {
                    searchSource.Add(prod);
                }
            }
        }

        public void searchName(String name)         //Letar upp önskad produkt baserat på namn och lägger till i söknings-källa
        {
            foreach (Product prod in stock)
            {
                if (prod.Name.IndexOf(name, StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    searchSource.Add(prod);
                }
            }
            stock.Filter = string.Format("nameCol LIKE 'name'");

        }

        public void searchType(String type)     //Letar upp önskad produkt baserat på typ och lägger till i söknings-källa
        {
            foreach (Product prod in stock)
            {
                if (prod.Type.IndexOf(type, StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    searchSource.Add(prod);
                }
            }
        }
    }
}
